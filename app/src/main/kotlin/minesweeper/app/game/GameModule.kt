package minesweeper.app.game

import dagger.Module
import dagger.Provides
import minesweeper.domain.TimerFactory
import javax.inject.Singleton

@Module
object GameModule {

    @JvmStatic
    @Provides
    @Singleton
    fun timerFactory(): TimerFactory {
        return TimerFactoryImpl()
    }

}