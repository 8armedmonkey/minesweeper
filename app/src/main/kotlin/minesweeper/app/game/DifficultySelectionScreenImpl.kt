package minesweeper.app.game

import minesweeper.app.ui.BaseScreen
import minesweeper.app.ui.UiConstants
import minesweeper.domain.Difficulties
import minesweeper.domain.Difficulty
import org.hexworks.zircon.api.Components
import org.hexworks.zircon.api.Positions
import org.hexworks.zircon.api.component.Button
import org.hexworks.zircon.api.grid.TileGrid
import org.hexworks.zircon.api.input.MouseAction
import org.hexworks.zircon.api.util.Consumer

internal class DifficultySelectionScreenImpl(tileGrid: TileGrid) :
    BaseScreen(tileGrid), DifficultySelectionScreen {

    companion object {
        const val IN_BETWEEN_BUTTONS_SPACING_CHARS = 2
    }

    override var difficultySelectionCallback: ((difficulty: Difficulty) -> Unit)? = null

    override fun prepare() {
        // No implementation.
    }

    override fun showDifficulties(difficulties: Difficulties) {
        difficulties.forEachIndexed { index, difficulty ->
            screen.addComponent(
                createDifficultySelectionButton(index, difficulty, difficulties.size())
            )
        }
    }

    private fun createDifficultySelectionButton(
        index: Int,
        difficulty: Difficulty,
        numberOfDifficulties: Int): Button {

        val totalButtonsHeight = numberOfDifficulties * UiConstants.BUTTON_HEIGHT_CHARS
        val totalSpacingsHeight = (numberOfDifficulties - 1) * IN_BETWEEN_BUTTONS_SPACING_CHARS
        val baseY = (screen.size().yLength - (totalButtonsHeight + totalSpacingsHeight)) / 2

        val x = (screen.size().xLength - difficulty.name.length - UiConstants.BUTTON_EXTRA_CHARS) / 2
        val y = baseY + index * (UiConstants.BUTTON_HEIGHT_CHARS + IN_BETWEEN_BUTTONS_SPACING_CHARS)

        val button = Components.button()
            .text(difficulty.name)
            .position(Positions.create(x, y))
            .build()

        button.onMousePressed(object : Consumer<MouseAction> {
            override fun accept(p: MouseAction) {
                difficultySelectionCallback?.invoke(difficulty)
            }
        })

        return button
    }

}