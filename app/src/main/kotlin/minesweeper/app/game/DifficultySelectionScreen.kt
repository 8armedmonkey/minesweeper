package minesweeper.app.game

import minesweeper.app.ui.UiScreen
import minesweeper.domain.Difficulties
import minesweeper.domain.Difficulty

interface DifficultySelectionScreen : UiScreen {

    var difficultySelectionCallback: ((difficulty: Difficulty) -> Unit)?

    fun showDifficulties(difficulties: Difficulties)

}