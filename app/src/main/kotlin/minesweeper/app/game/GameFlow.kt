package minesweeper.app.game


import minesweeper.app.Injector
import minesweeper.app.config.ConfigurationRepository
import minesweeper.app.flow.Flow
import minesweeper.app.flow.FlowController
import minesweeper.app.resource.StringIds
import minesweeper.app.resource.StringResource
import minesweeper.app.ui.UiService
import minesweeper.domain.Difficulty
import minesweeper.domain.Minesweeper
import minesweeper.domain.TimerFactory
import javax.inject.Inject

class GameFlow internal constructor() : Flow() {

    @Inject
    internal lateinit var flowController: FlowController

    @Inject
    internal lateinit var configurationRepository: ConfigurationRepository

    @Inject
    internal lateinit var uiService: UiService

    @Inject
    internal lateinit var timerFactory: TimerFactory

    @Inject
    internal lateinit var stringResource: StringResource

    private lateinit var minesweeper: Minesweeper
    private lateinit var screen: GameScreen

    init {
        Injector.applicationComponent.inject(this)
    }

    override fun start(vararg args: Any) {
        screen = uiService.createScreen(GameScreen::class)

        if (args.isEmpty() || args[0] !is Difficulty) {
            showMissingDifficultyErrorMessage()
            return
        }

        initMinesweeper(args[0] as Difficulty)
        initScreen()

        updateScreen()
    }

    private fun initMinesweeper(difficulty: Difficulty) {
        val configuration = configurationRepository.retrieveConfiguration()

        minesweeper = Minesweeper.Builder()
            .difficulties(configuration.difficulties)
            .timerFactory(timerFactory)
            .build()

        minesweeper.init(difficulty)
    }

    private fun initScreen() {
        screen.onCellOpenedCallback = { index ->
            minesweeper.openCellAt(index)
            updateScreen()
        }

        screen.onCellFlagToggledCallback = { index ->
            minesweeper.toggleFlagCellAt(index)
            updateScreen()
        }

        screen.onResetGameRequestedCallback = {
            minesweeper.reset()
            updateScreen()
        }

        screen.onLeaderBoardRequestedCallback = {
            flowController.start(LeaderBoardFlow::class)
        }
    }

    private fun updateScreen() {
        screen.showBoard(minesweeper.boardInfo)
        screen.showRemainingTime(minesweeper.remainingTimeMillis)

        if (minesweeper.status == Minesweeper.Status.WON) {
            screen.showWinningMessage()

        } else if (minesweeper.status == Minesweeper.Status.LOST) {
            screen.showLosingMessage()
        }
    }

    private fun showMissingDifficultyErrorMessage() {
        screen.showErrorMessage(
            stringResource.getString(StringIds.ERR_INIT_DIFFICULTY_NOT_SPECIFIED)
        )
    }

}