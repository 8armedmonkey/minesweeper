package minesweeper.app.game

import minesweeper.domain.Timer
import minesweeper.domain.TimerFactory

internal class TimerFactoryImpl : TimerFactory {

    override fun createTimer(durationMillis: Long): Timer {
        return SimpleTimer(durationMillis)
    }

}