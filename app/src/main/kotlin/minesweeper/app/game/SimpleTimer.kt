package minesweeper.app.game

import minesweeper.domain.Timer
import java.util.concurrent.TimeUnit

internal class SimpleTimer(
    override val durationMillis: Long
) : Timer {

    override val remainingTimeMillis: Long
        get() {
            val currentStartTimeMillis = startTimeMillis

            return if (currentStartTimeMillis != null) {
                durationMillis - (currentTimeMillis - currentStartTimeMillis) - elapsedTimeMillis
            } else {
                durationMillis - elapsedTimeMillis
            }
        }

    override val running: Boolean
        get() = startTimeMillis != null

    override var onTimerTickCallback: ((Timer) -> Unit)? = null

    override var onTimerExpiredCallback: ((Timer) -> Unit)? = null

    private val currentTimeMillis: Long
        get() = TimeUnit.NANOSECONDS.toMillis(System.nanoTime())

    private var elapsedTimeMillis: Long = 0

    private var startTimeMillis: Long? = null

    override fun start() {
        synchronized(this) {
            if (startTimeMillis == null) {
                startTimeMillis = currentTimeMillis
            }
        }
    }

    override fun stop() {
        synchronized(this) {
            val currentStartTimeMillis = startTimeMillis

            if (currentStartTimeMillis != null) {
                elapsedTimeMillis = currentTimeMillis - currentStartTimeMillis
                startTimeMillis = null
            }
        }
    }

    override fun reset() {
        synchronized(this) {
            elapsedTimeMillis = 0
            startTimeMillis = null
        }
    }

}