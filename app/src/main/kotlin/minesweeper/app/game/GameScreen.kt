package minesweeper.app.game

import minesweeper.app.ui.UiScreen
import minesweeper.domain.BoardInfo

interface GameScreen : UiScreen {

    var onCellOpenedCallback: ((index: Int) -> Unit)?

    var onCellFlagToggledCallback: ((index: Int) -> Unit)?

    var onResetGameRequestedCallback: ((Unit) -> Unit)?

    var onLeaderBoardRequestedCallback: ((Unit) -> Unit)?

    fun showRemainingTime(remainingTimeMillis: Long)

    fun showBoard(boardInfo: BoardInfo)

    fun showWinningMessage()

    fun showLosingMessage()

    fun showErrorMessage(errorMessage: String)

}