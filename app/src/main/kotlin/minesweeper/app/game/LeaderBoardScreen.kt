package minesweeper.app.game

import minesweeper.app.ui.UiScreen
import minesweeper.domain.LeaderBoard

interface LeaderBoardScreen : UiScreen {

    fun showStats(leaderBoard: LeaderBoard)

}