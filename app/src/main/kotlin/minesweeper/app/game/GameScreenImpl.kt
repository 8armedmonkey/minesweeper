package minesweeper.app.game

import minesweeper.app.ui.BaseScreen
import minesweeper.domain.BoardInfo
import org.hexworks.zircon.api.grid.TileGrid

internal class GameScreenImpl(tileGrid: TileGrid) : BaseScreen(tileGrid), GameScreen {

    override var onCellOpenedCallback: ((index: Int) -> Unit)? = null
    override var onCellFlagToggledCallback: ((index: Int) -> Unit)? = null
    override var onResetGameRequestedCallback: ((Unit) -> Unit)? = null
    override var onLeaderBoardRequestedCallback: ((Unit) -> Unit)? = null

    override fun prepare() {
        // No implementation.
    }

    override fun showRemainingTime(remainingTimeMillis: Long) {
        TODO("not implemented")
    }

    override fun showBoard(boardInfo: BoardInfo) {
        TODO("not implemented")
    }

    override fun showWinningMessage() {
        TODO("not implemented")
    }

    override fun showLosingMessage() {
        TODO("not implemented")
    }

    override fun showErrorMessage(errorMessage: String) {
        TODO("not implemented")
    }

}