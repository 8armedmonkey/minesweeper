package minesweeper.app.game

import minesweeper.app.Injector
import minesweeper.app.config.ConfigurationRepository
import minesweeper.app.flow.Flow
import minesweeper.app.flow.FlowController
import minesweeper.app.ui.UiService
import javax.inject.Inject

class DifficultySelectionFlow internal constructor() : Flow() {

    @Inject
    internal lateinit var flowController: FlowController

    @Inject
    internal lateinit var configurationRepository: ConfigurationRepository

    @Inject
    internal lateinit var uiService: UiService

    init {
        Injector.applicationComponent.inject(this)
    }

    override fun start(vararg args: Any) {
        val configuration = configurationRepository.retrieveConfiguration()
        val screen = uiService.createScreen(DifficultySelectionScreen::class)

        screen.difficultySelectionCallback = { difficulty ->
            flowController.start(GameFlow::class, difficulty)
        }

        screen.display()
        screen.showDifficulties(configuration.difficulties)
    }

}