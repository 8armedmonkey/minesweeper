package minesweeper.app.ui

interface UiScreen {

    fun display()

}