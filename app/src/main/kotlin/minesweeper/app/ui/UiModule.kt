package minesweeper.app.ui

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object UiModule {

    @JvmStatic
    @Provides
    @Singleton
    fun uiService(): UiService {
        return UiServiceImpl()
    }

}