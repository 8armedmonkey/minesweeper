package minesweeper.app.ui

import org.hexworks.zircon.api.Screens
import org.hexworks.zircon.api.grid.TileGrid
import org.hexworks.zircon.api.screen.Screen

abstract class BaseScreen(tileGrid: TileGrid) : UiScreen {

    protected val screen: Screen = Screens.createScreenFor(tileGrid)

    override fun display() {
        screen.display()
    }

    abstract fun prepare()

}