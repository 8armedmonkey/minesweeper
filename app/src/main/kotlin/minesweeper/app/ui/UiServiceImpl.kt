package minesweeper.app.ui

import minesweeper.app.game.DifficultySelectionScreen
import minesweeper.app.game.DifficultySelectionScreenImpl
import minesweeper.app.game.GameScreen
import minesweeper.app.game.GameScreenImpl
import org.hexworks.zircon.api.AppConfigs
import org.hexworks.zircon.api.Sizes
import org.hexworks.zircon.api.SwingApplications
import org.hexworks.zircon.api.grid.TileGrid
import kotlin.reflect.KClass
import kotlin.reflect.full.cast

internal class UiServiceImpl : UiService {

    private lateinit var tileGrid: TileGrid

    override fun init(windowSize: Size) {
        tileGrid = SwingApplications.startTileGrid(
            AppConfigs.newConfig()
                .defaultSize(Sizes.create(windowSize.width, windowSize.height))
                .build()
        )
    }

    override fun <T : UiScreen> createScreen(screenClass: KClass<T>): T {
        return when (screenClass) {
            DifficultySelectionScreen::class -> screenClass.cast(DifficultySelectionScreenImpl(tileGrid))
            GameScreen::class -> screenClass.cast(GameScreenImpl(tileGrid))
            else -> throw UnsupportedScreenException()
        }
    }

}