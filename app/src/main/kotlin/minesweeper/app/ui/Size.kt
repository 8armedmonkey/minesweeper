package minesweeper.app.ui

data class Size(
    val width: Int,
    val height: Int
)