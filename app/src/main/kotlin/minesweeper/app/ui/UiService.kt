package minesweeper.app.ui

import kotlin.reflect.KClass

interface UiService {

    fun init(windowSize: Size)

    fun <T : UiScreen> createScreen(screenClass: KClass<T>): T

}