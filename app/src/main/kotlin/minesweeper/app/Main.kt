package minesweeper.app

fun main(args: Array<String>) {
    Injector.applicationComponent
        .application()
        .start()
}