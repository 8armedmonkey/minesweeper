package minesweeper.app.resource

interface StringResource {

    fun getString(key: String): String

}