package minesweeper.app.resource

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object ResourceModule {

    @JvmStatic
    @Provides
    @Singleton
    fun stringResource(): StringResource {
        return StringResourceImpl()
    }

}