package minesweeper.app.resource

class StringResourceImpl : StringResource {

    companion object {
        private val strings: Map<String, String> = mapOf(
            Pair(StringIds.ERR_INIT_DIFFICULTY_NOT_SPECIFIED, "Initialization Error: Difficulty is not specified.")
        )
    }

    override fun getString(key: String): String {
        return strings[key].orEmpty()
    }

}