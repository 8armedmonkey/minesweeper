package minesweeper.app

internal object Injector {

    internal val applicationComponent: ApplicationComponent = DaggerApplicationComponent.create()

}