package minesweeper.app

import dagger.Component
import minesweeper.app.config.ConfigurationFlow
import minesweeper.app.config.ConfigurationModule
import minesweeper.app.flow.FlowModule
import minesweeper.app.game.DifficultySelectionFlow
import minesweeper.app.game.GameFlow
import minesweeper.app.game.GameModule
import minesweeper.app.resource.ResourceModule
import minesweeper.app.ui.UiModule
import javax.inject.Singleton

@Component(modules = [
    ApplicationModule::class,
    ConfigurationModule::class,
    FlowModule::class,
    GameModule::class,
    ResourceModule::class,
    UiModule::class
])
@Singleton
interface ApplicationComponent {

    fun application(): Application

    fun inject(flow: ConfigurationFlow)

    fun inject(flow: DifficultySelectionFlow)

    fun inject(flow: GameFlow)

}