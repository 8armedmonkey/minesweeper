package minesweeper.app

import minesweeper.app.config.ConfigurationFlow
import minesweeper.app.flow.FlowController

class Application private constructor(
    private val flowController: FlowController
) {

    fun start() {
        flowController.start(ConfigurationFlow::class)
    }

    internal class Builder {

        private lateinit var flowController: FlowController

        internal fun flowController(flowController: FlowController): Builder {
            this.flowController = flowController
            return this
        }

        internal fun build(): Application {
            return Application(flowController)
        }

    }

}