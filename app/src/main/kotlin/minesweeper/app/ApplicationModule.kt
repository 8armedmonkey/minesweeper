package minesweeper.app

import dagger.Module
import dagger.Provides
import minesweeper.app.flow.FlowController
import javax.inject.Singleton

@Module
object ApplicationModule {

    @JvmStatic
    @Provides
    @Singleton
    fun application(flowController: FlowController): Application {
        return Application.Builder()
            .flowController(flowController)
            .build()
    }

}