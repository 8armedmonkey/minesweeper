package minesweeper.app.config

internal class ConfigurationRepositoryImpl(
    private val configurationReader: ConfigurationReader

) : ConfigurationRepository {

    private lateinit var configuration: Configuration

    override fun retrieveConfiguration(): Configuration {
        synchronized(this) {
            if (!::configuration.isInitialized) {
                configuration = configurationReader.read()
            }
            return configuration
        }
    }

}