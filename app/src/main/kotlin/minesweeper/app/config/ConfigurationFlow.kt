package minesweeper.app.config

import minesweeper.app.Injector
import minesweeper.app.flow.Flow
import minesweeper.app.flow.FlowController
import minesweeper.app.game.DifficultySelectionFlow
import minesweeper.app.ui.UiService
import javax.inject.Inject

class ConfigurationFlow internal constructor() : Flow() {

    @Inject
    internal lateinit var flowController: FlowController

    @Inject
    internal lateinit var configurationRepository: ConfigurationRepository

    @Inject
    internal lateinit var uiService: UiService

    init {
        Injector.applicationComponent.inject(this)
    }

    override fun start(vararg args: Any) {
        val configuration = configurationRepository.retrieveConfiguration()
        uiService.init(configuration.windowSize)

        flowController.start(DifficultySelectionFlow::class)
    }

}