package minesweeper.app.config

import minesweeper.app.ui.Size
import minesweeper.domain.Difficulties

data class Configuration(
    val difficulties: Difficulties,
    val windowSize: Size
)