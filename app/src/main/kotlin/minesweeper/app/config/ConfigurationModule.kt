package minesweeper.app.config

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object ConfigurationModule {

    private const val CONFIG_FILE_PATH = "config.properties"

    @JvmStatic
    @Provides
    @Singleton
    fun configurationRepository(): ConfigurationRepository {
        return ConfigurationRepositoryImpl(PropertiesConfigurationReader(CONFIG_FILE_PATH))
    }

}