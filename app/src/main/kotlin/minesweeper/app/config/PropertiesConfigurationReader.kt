package minesweeper.app.config

import minesweeper.app.ui.Size
import minesweeper.domain.BoardSize
import minesweeper.domain.Difficulties
import minesweeper.domain.Difficulty
import java.io.File
import java.io.FileReader
import java.util.*

class PropertiesConfigurationReader(
    private val configurationFilePath: String

) : ConfigurationReader {

    companion object {
        private const val KEY_DIFFICULTIES = "difficulties"
        private const val KEY_DIFFICULTY_BOARD_SIZE_TEMPLATE = "difficulty.%s.boardSize"
        private const val KEY_DIFFICULTY_NUMBER_OF_MINES_TEMPLATE = "difficulty.%s.numberOfMines"
        private const val KEY_DIFFICULTY_DURATION_MILLIS_TEMPLATE = "difficulty.%s.durationMillis"
        private const val KEY_WINDOW_SIZE = "window.size"
        private const val DELIMITER_FOR_VALUES = ','
    }

    override fun read(): Configuration {
        val file = File(this::class.java.classLoader.getResource("../resources/$configurationFilePath").toURI())

        val properties = Properties()
        properties.load(FileReader(file))

        val difficultiesList = properties.getProperty(KEY_DIFFICULTIES)
            .split(DELIMITER_FOR_VALUES)
            .fold(mutableListOf<Difficulty>()) { list, difficultyName ->
                list.add(Difficulty(
                    name = difficultyName,
                    boardSize = parseBoardSize(properties.getProperty(KEY_DIFFICULTY_BOARD_SIZE_TEMPLATE.format(difficultyName))),
                    numberOfMines = properties.getProperty(KEY_DIFFICULTY_NUMBER_OF_MINES_TEMPLATE.format(difficultyName)).toInt(),
                    durationMillis = properties.getProperty(KEY_DIFFICULTY_DURATION_MILLIS_TEMPLATE.format(difficultyName)).toLong()
                ))
                list
            }

        val windowSize = parseWindowSize(properties.getProperty(KEY_WINDOW_SIZE))

        return Configuration(
            Difficulties(*difficultiesList.toTypedArray()),
            windowSize
        )
    }

    private fun parseBoardSize(boardSizeValue: String): BoardSize {
        val (width, height) = boardSizeValue.split(DELIMITER_FOR_VALUES).map { it.toInt() }
        return BoardSize(width, height)
    }

    private fun parseWindowSize(windowSizeValue: String): Size {
        val (width, height) = windowSizeValue.split(DELIMITER_FOR_VALUES).map { it.toInt() }
        return Size(width, height)
    }

}