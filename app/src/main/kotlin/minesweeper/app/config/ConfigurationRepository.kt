package minesweeper.app.config

interface ConfigurationRepository {

    fun retrieveConfiguration(): Configuration

}