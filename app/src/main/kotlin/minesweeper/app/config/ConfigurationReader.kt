package minesweeper.app.config

interface ConfigurationReader {

    fun read(): Configuration

}