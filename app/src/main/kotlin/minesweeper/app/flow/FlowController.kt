package minesweeper.app.flow

import kotlin.reflect.KClass

interface FlowController {

    fun <T : Flow> start(flowClass: KClass<T>, vararg args: Any)

}