package minesweeper.app.flow

class FlowInitException(message: String) : RuntimeException(message)