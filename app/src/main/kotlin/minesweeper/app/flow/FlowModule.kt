package minesweeper.app.flow

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object FlowModule {

    @JvmStatic
    @Provides
    @Singleton
    fun flowController(): FlowController {
        return FlowControllerImpl()
    }

}