package minesweeper.app.flow

abstract class Flow {

    abstract fun start(vararg args: Any)

}