package minesweeper.app.flow

import kotlin.reflect.KClass
import kotlin.reflect.KVisibility

internal class FlowControllerImpl : FlowController {

    override fun <T : Flow> start(flowClass: KClass<T>, vararg args: Any) {
        val constructor = flowClass.constructors.find { it.parameters.isEmpty() }

        when (constructor) {
            null -> throw FlowInitException("The $flowClass does not contain default constructor.")
            else -> when (constructor.visibility) {
                null, KVisibility.PRIVATE ->
                    throw FlowInitException("The $flowClass default constructor is not accessible.")

                else -> constructor.call().start(args)
            }
        }
    }

}