package minesweeper.app.test.data

import minesweeper.app.config.Configuration
import minesweeper.app.ui.Size
import minesweeper.domain.BoardSize
import minesweeper.domain.Difficulties
import minesweeper.domain.Difficulty

object TestData {

    fun getConfiguration(): Configuration {
        return Configuration(
            getDifficulties(),
            getWindowSize()
        )
    }

    private fun getDifficulties(): Difficulties {
        return Difficulties(
            Difficulty("Easy", BoardSize(1, 1), 0, 10000),
            Difficulty("Normal", BoardSize(3, 3), 2, 30000),
            Difficulty("Impossible", BoardSize(1, 1), 1, 1000)
        )
    }

    private fun getWindowSize(): Size {
        return Size(30, 40)
    }

}