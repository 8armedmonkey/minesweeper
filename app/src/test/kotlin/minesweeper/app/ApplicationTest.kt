package minesweeper.app

import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import minesweeper.app.config.ConfigurationFlow
import minesweeper.app.flow.Flow
import minesweeper.app.flow.FlowController
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.reflect.KClass

class ApplicationTest {

    private lateinit var flowController: FlowController
    private lateinit var application: Application

    @BeforeEach
    fun setUp() {
        flowController = mock()

        application = Application.Builder()
            .flowController(flowController)
            .build()
    }

    @Test
    fun start_configurationFlowIsStarted() {
        application.start()

        argumentCaptor<KClass<Flow>>().apply {
            verify(flowController).start(capture())

            assertEquals(ConfigurationFlow::class, firstValue)
        }
    }

}