package minesweeper.app.config

import minesweeper.domain.BoardSize
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class PropertiesConfigurationReaderTest {

    private lateinit var configurationReader: ConfigurationReader

    @BeforeEach
    fun setUp() {
        configurationReader = PropertiesConfigurationReader("config.properties")
    }

    @Test
    fun parseDifficulties_difficultyNamesAreParsedCorrectly() {
        val configuration = configurationReader.read()

        assertEquals(3, configuration.difficulties.size())
        assertEquals("easy", configuration.difficulties.get(0).name)
        assertEquals("normal", configuration.difficulties.get(1).name)
        assertEquals("hard", configuration.difficulties.get(2).name)
    }

    @Test
    fun parseDifficultyAttributes_difficultyAttributesAreParsedCorrectly() {
        val configuration = configurationReader.read()

        assertEquals(BoardSize(8, 12), configuration.difficulties.get(0).boardSize)
        assertEquals(24, configuration.difficulties.get(0).numberOfMines)
        assertEquals(600000, configuration.difficulties.get(0).durationMillis)

        assertEquals(BoardSize(12, 18), configuration.difficulties.get(1).boardSize)
        assertEquals(54, configuration.difficulties.get(1).numberOfMines)
        assertEquals(900000, configuration.difficulties.get(1).durationMillis)

        assertEquals(BoardSize(18, 27), configuration.difficulties.get(2).boardSize)
        assertEquals(121, configuration.difficulties.get(2).numberOfMines)
        assertEquals(1800000, configuration.difficulties.get(2).durationMillis)
    }

    @Test
    fun parseWindowSize_windowSizeIsParsedCorrectly() {
        val configuration = configurationReader.read()

        assertEquals(30, configuration.windowSize.width)
        assertEquals(40, configuration.windowSize.height)
    }

}