package minesweeper.app.config

import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import minesweeper.app.flow.Flow
import minesweeper.app.flow.FlowController
import minesweeper.app.game.DifficultySelectionFlow
import minesweeper.app.test.data.TestData
import minesweeper.app.ui.Size
import minesweeper.app.ui.UiService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.reflect.KClass

class ConfigurationFlowTest {

    private lateinit var flowController: FlowController
    private lateinit var configurationRepository: ConfigurationRepository
    private lateinit var configuration: Configuration
    private lateinit var uiService: UiService
    private lateinit var configurationFlow: ConfigurationFlow

    @BeforeEach
    fun setUp() {
        flowController = mock()
        configurationRepository = mock()
        configuration = TestData.getConfiguration()
        uiService = mock()
        configurationFlow = ConfigurationFlow()
        configurationFlow.flowController = flowController
        configurationFlow.configurationRepository = configurationRepository
        configurationFlow.uiService = uiService

        whenever(configurationRepository.retrieveConfiguration()).thenReturn(configuration)
    }

    @Test
    fun start_configurationIsRead() {
        configurationFlow.start()

        verify(configurationRepository).retrieveConfiguration()
    }

    @Test
    fun start_uiIsInitialized() {
        configurationFlow.start()

        argumentCaptor<Size>().apply {
            verify(uiService).init(capture())

            assertEquals(configuration.windowSize, firstValue)
        }
    }

    @Test
    fun start_difficultySelectionIsStarted() {
        configurationFlow.start()

        argumentCaptor<KClass<Flow>>().apply {
            verify(flowController).start(capture())

            assertEquals(DifficultySelectionFlow::class, firstValue)
        }
    }

}