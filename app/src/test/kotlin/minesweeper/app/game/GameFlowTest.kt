package minesweeper.app.game

import com.nhaarman.mockito_kotlin.*
import minesweeper.app.config.Configuration
import minesweeper.app.config.ConfigurationRepository
import minesweeper.app.flow.FlowController
import minesweeper.app.resource.StringResource
import minesweeper.app.test.data.TestData
import minesweeper.app.ui.UiScreen
import minesweeper.app.ui.UiService
import minesweeper.domain.Timer
import minesweeper.domain.TimerFactory
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.reflect.KClass

class GameFlowTest {

    private lateinit var flowController: FlowController
    private lateinit var configurationRepository: ConfigurationRepository
    private lateinit var configuration: Configuration
    private lateinit var uiService: UiService
    private lateinit var timerFactory: TimerFactory
    private lateinit var timer: Timer
    private lateinit var stringResource: StringResource
    private lateinit var screen: GameScreen
    private lateinit var gameFlow: GameFlow

    @BeforeEach
    fun setUp() {
        flowController = mock()
        configurationRepository = mock()
        configuration = TestData.getConfiguration()
        uiService = mock()
        timerFactory = mock()
        timer = mock()
        stringResource = mock()
        screen = mock()

        gameFlow = GameFlow()
        gameFlow.flowController = flowController
        gameFlow.configurationRepository = configurationRepository
        gameFlow.uiService = uiService
        gameFlow.timerFactory = timerFactory
        gameFlow.stringResource = stringResource

        whenever(configurationRepository.retrieveConfiguration()).thenReturn(configuration)
        whenever(uiService.createScreen<GameScreen>(any())).thenReturn(screen)
        whenever(timerFactory.createTimer(any())).thenReturn(timer)
        whenever(stringResource.getString(any())).thenReturn("")
    }

    @Test
    fun start_screenIsCreated() {
        gameFlow.start(TestData.getConfiguration().difficulties.get(0))

        argumentCaptor<KClass<UiScreen>>().apply {
            verify(uiService).createScreen(capture())

            Assertions.assertEquals(GameScreen::class, firstValue)
        }
    }

    @Test
    fun start_noDifficultySelection_showErrorMessage() {
        gameFlow.start()

        verify(screen).showErrorMessage(any())
    }

    @Test
    fun start_boardInfoIsShown() {
        gameFlow.start(TestData.getConfiguration().difficulties.get(0))

        verify(screen).showBoard(any())
    }

    @Test
    fun start_remainingTimeIsShown() {
        gameFlow.start(TestData.getConfiguration().difficulties.get(0))

        verify(screen).showRemainingTime(any())
    }

}