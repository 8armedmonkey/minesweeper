package minesweeper.app.game

import com.nhaarman.mockito_kotlin.*
import minesweeper.app.config.Configuration
import minesweeper.app.config.ConfigurationRepository
import minesweeper.app.flow.FlowController
import minesweeper.app.test.data.TestData
import minesweeper.app.ui.UiScreen
import minesweeper.app.ui.UiService
import minesweeper.domain.Difficulties
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.reflect.KClass

class DifficultySelectionFlowTest {

    private lateinit var flowController: FlowController
    private lateinit var configurationRepository: ConfigurationRepository
    private lateinit var configuration: Configuration
    private lateinit var uiService: UiService
    private lateinit var screen: DifficultySelectionScreen
    private lateinit var difficultySelectionFlow: DifficultySelectionFlow

    @BeforeEach
    fun setUp() {
        flowController = mock()
        configurationRepository = mock()
        configuration = TestData.getConfiguration()
        uiService = mock()
        screen = mock()
        difficultySelectionFlow = DifficultySelectionFlow()
        difficultySelectionFlow.flowController = flowController
        difficultySelectionFlow.configurationRepository = configurationRepository
        difficultySelectionFlow.uiService = uiService

        whenever(configurationRepository.retrieveConfiguration()).thenReturn(configuration)
        whenever(uiService.createScreen<DifficultySelectionScreen>(any())).thenReturn(screen)
    }

    @Test
    fun start_configIsRetrievedFromRepository() {
        difficultySelectionFlow.start()

        verify(configurationRepository).retrieveConfiguration()
    }

    @Test
    fun start_screenIsCreated() {
        difficultySelectionFlow.start()

        argumentCaptor<KClass<UiScreen>>().apply {
            verify(uiService).createScreen(capture())

            assertEquals(DifficultySelectionScreen::class, firstValue)
        }
    }

    @Test
    fun start_difficultiesAreShown() {
        difficultySelectionFlow.start()

        argumentCaptor<Difficulties>().apply {
            verify(screen).showDifficulties(capture())

            assertEquals(configuration.difficulties, firstValue)
        }
    }

}