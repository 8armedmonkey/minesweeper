package minesweeper.app.game

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.lang.Thread.sleep

class SimpleTimerTest {

    private lateinit var timer: SimpleTimer

    @BeforeEach
    fun setUp() {
        timer = SimpleTimer(5000)
        timer.durationMillis
    }

    @Test
    fun start_timerIsStarted() {
        timer.start()

        assertTrue(timer.running)
    }

    @Test
    fun start_remainingTimeIsDecreased() {
        timer.start()
        sleep(2000)

        assertEquals(3000.0, timer.remainingTimeMillis.toDouble(), 100.0)
    }

    @Test
    fun stop_timerIsStopped() {
        timer.start()
        timer.stop()

        assertFalse(timer.running)
    }

    @Test
    fun stop_remainingTimeIsNotDecreasedAnymore() {
        timer.start()
        sleep(2000)

        timer.stop()
        sleep(2000)

        assertEquals(3000.0, timer.remainingTimeMillis.toDouble(), 100.0)
    }

    @Test
    fun reset_remainingTimeShouldBeEqToDuration() {
        timer.start()
        sleep(1000)

        timer.reset()

        assertEquals(timer.durationMillis, timer.remainingTimeMillis)
    }

}