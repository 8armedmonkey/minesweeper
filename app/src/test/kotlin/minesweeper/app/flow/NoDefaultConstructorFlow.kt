package minesweeper.app.flow

internal class NoDefaultConstructorFlow internal constructor(
    private val field1: Any,
    private val field2: Any
) : Flow() {

    override fun start(vararg args: Any) {
        println("Flow constructed with field1: $field1 and field2: $field2.")
    }

}