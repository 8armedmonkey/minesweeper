package minesweeper.app.flow

internal class InaccessibleDefaultConstructorFlow private constructor() : Flow() {

    override fun start(vararg args: Any) {
        // No implementation.
    }

}