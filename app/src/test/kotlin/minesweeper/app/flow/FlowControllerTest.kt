package minesweeper.app.flow

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class FlowControllerTest {

    private lateinit var flowController: FlowController

    @BeforeEach
    fun setUp() {
        flowController = FlowControllerImpl()
    }

    @Test
    fun start_flowClassHasDefaultConstructor_flowIsStarted() {
        flowController.start(WithDefaultConstructorFlow::class)
    }

    @Test
    fun start_flowClassHasUnaccessibleDefaultConstructor_flowInitExceptionIsThrown() {
        assertThrows<FlowInitException> {
            flowController.start(InaccessibleDefaultConstructorFlow::class)
        }
    }

    @Test
    fun start_flowClassHasNoDefaultConstructor_flowInitExceptionIsThrown() {
        assertThrows<FlowInitException> {
            flowController.start(NoDefaultConstructorFlow::class)
        }
    }

}