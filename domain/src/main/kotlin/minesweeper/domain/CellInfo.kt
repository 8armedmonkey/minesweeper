package minesweeper.domain

data class CellInfo(
    val hasMine: Boolean,
    val opened: Boolean,
    val flagged: Boolean,
    val indicator: Int
)