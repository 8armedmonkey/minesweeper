package minesweeper.domain

class NotInitializedException : RuntimeException()