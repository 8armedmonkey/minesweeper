package minesweeper.domain

class InvalidDifficultyException : RuntimeException()