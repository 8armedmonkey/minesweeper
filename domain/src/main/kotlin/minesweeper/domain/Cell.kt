package minesweeper.domain

class Cell private constructor(
    val hasMine: Boolean,
    opened: Boolean,
    flagged: Boolean
) {

    val indicator: Int
        get() {
            return neighbors.values.filter { it.hasMine }.size
        }

    val numberOfNeighbors: Int
        get() {
            return neighbors.size
        }

    var opened: Boolean = opened
        private set

    var flagged: Boolean = flagged
        private set

    private val neighbors: MutableMap<NeighborAt, Cell> = mutableMapOf()

    fun open() {
        if (!opened) {
            opened = true

            if (!hasMine && indicator == 0) {
                neighbors.values
                    .filter { !it.hasMine && !it.opened }
                    .forEach { it.open() }
            }
        }
    }

    fun toggleFlag() {
        flagged = !flagged
    }

    fun neighborAt(at: NeighborAt): Cell? {
        return neighbors[at]
    }

    private fun neighborAt(at: NeighborAt, neighbor: Cell) {
        neighbors[at] = neighbor
    }

    enum class NeighborAt {
        NORTH, NORTH_EAST, EAST, SOUTH_EAST, SOUTH, SOUTH_WEST, WEST, NORTH_WEST
    }

    class Builder {

        private var hasMine: Boolean = false
        private var opened: Boolean = false
        private var flagged: Boolean = false

        fun hasMine(hasMine: Boolean): Builder {
            this.hasMine = hasMine
            return this
        }

        fun opened(opened: Boolean): Builder {
            this.opened = opened
            return this
        }

        fun flagged(flagged: Boolean): Builder {
            this.flagged = flagged
            return this
        }

        fun build(): Cell {
            return Cell(hasMine, opened, flagged)
        }

    }

    class Connector {

        companion object {

            fun northToSouth(north: Cell, south: Cell) {
                north.neighborAt(NeighborAt.SOUTH, south)
                south.neighborAt(NeighborAt.NORTH, north)
            }

            fun westToEast(west: Cell, east: Cell) {
                west.neighborAt(NeighborAt.EAST, east)
                east.neighborAt(NeighborAt.WEST, west)
            }

            fun northWestToSouthEast(northWest: Cell, southEast: Cell) {
                northWest.neighborAt(NeighborAt.SOUTH_EAST, southEast)
                southEast.neighborAt(NeighborAt.NORTH_WEST, northWest)
            }

            fun northEastToSouthWest(northEast: Cell, southWest: Cell) {
                northEast.neighborAt(NeighborAt.SOUTH_WEST, southWest)
                southWest.neighborAt(NeighborAt.NORTH_EAST, northEast)
            }

        }

    }

}