package minesweeper.domain

interface TimerFactory {

    fun createTimer(durationMillis: Long): Timer

}