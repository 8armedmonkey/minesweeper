package minesweeper.domain

data class TimerTickEvent(
    val remainingTimeMillis: Long
)