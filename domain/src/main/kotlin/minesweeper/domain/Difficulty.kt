package minesweeper.domain

data class Difficulty(
    val name: String,
    val boardSize: BoardSize,
    val numberOfMines: Int,
    val durationMillis: Long
)