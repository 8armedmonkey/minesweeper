package minesweeper.domain

interface StatsRepository {

    fun getAll(): LeaderBoard

    fun add(stats: Stats)

    fun clear()

}