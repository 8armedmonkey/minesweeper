package minesweeper.domain

class Minesweeper private constructor(
    private val difficulties: Difficulties,
    private val timerFactory: TimerFactory
) {

    val status: Status
        get() {
            assertBoardInitialized()
            return when {
                board.hasBeenCompleted -> Status.WON
                board.hasBeenExploded || timer.remainingTimeMillis == 0L -> Status.LOST
                else -> Status.PLAYING
            }
        }

    val boardInfo: BoardInfo
        get() {
            assertBoardInitialized()
            return BoardInfo(
                board.size,
                (0 until board.size.numberOfCells)
                    .toList()
                    .fold(mutableListOf()) { list, index ->
                        list.add(board.cellInfoAt(index))
                        list
                    }
            )
        }

    val durationMillis: Long
        get() {
            assertTimerInitialized()
            return timer.durationMillis
        }

    val remainingTimeMillis: Long
        get() {
            assertTimerInitialized()
            return timer.remainingTimeMillis
        }

    private val isAllowedToStartTimer: Boolean
        get() = status == Status.PLAYING

    private val isAllowedToPerformAction: Boolean
        get() = status == Status.PLAYING && timer.running

    private lateinit var board: Board
    private lateinit var timer: Timer

    fun init(difficulty: Difficulty) {
        assertDifficultyExists(difficulty)

        board = Board.Builder()
            .size(difficulty.boardSize)
            .numberOfMines(difficulty.numberOfMines)
            .build()

        timer = timerFactory.createTimer(difficulty.durationMillis)

        board.init()
    }

    fun start() {
        assertInitialized()

        if (isAllowedToStartTimer) {
            timer.start()
        }
    }

    fun stop() {
        assertInitialized()

        if (isAllowedToPerformAction) {
            timer.stop()
        }
    }

    fun reset() {
        assertInitialized()

        board.init()
        timer.reset()
    }

    fun openCellAt(index: Int) {
        assertInitialized()

        if (isAllowedToPerformAction) {
            board.openCellAt(index)

            if (board.status != Board.Status.INCOMPLETE) {
                timer.stop()
            }
        }
    }

    fun toggleFlagCellAt(index: Int) {
        assertInitialized()

        if (isAllowedToPerformAction) {
            board.toggleFlagCellAt(index)
        }
    }

    private fun assertDifficultyExists(difficulty: Difficulty) {
        if (!difficulties.contains(difficulty)) {
            throw InvalidDifficultyException()
        }
    }

    private fun assertBoardInitialized() {
        if (!::board.isInitialized) {
            throw NotInitializedException()
        }
    }

    private fun assertTimerInitialized() {
        if (!::timer.isInitialized) {
            throw NotInitializedException()
        }
    }

    private fun assertInitialized() {
        assertBoardInitialized()
        assertTimerInitialized()
    }

    enum class Status {
        PLAYING, WON, LOST
    }

    class Builder {

        private lateinit var difficulties: Difficulties
        private lateinit var timerFactory: TimerFactory

        fun difficulties(difficulties: Difficulties): Builder {
            this.difficulties = difficulties
            return this
        }

        fun timerFactory(timerFactory: TimerFactory): Builder {
            this.timerFactory = timerFactory
            return this
        }

        fun build(): Minesweeper {
            return Minesweeper(difficulties, timerFactory)
        }

    }

}