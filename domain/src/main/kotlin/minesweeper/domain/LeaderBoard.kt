package minesweeper.domain

class LeaderBoard(vararg stats: Stats) : Iterable<Stats> {

    private val entries: List<Stats> = stats.toList()

    override fun iterator(): Iterator<Stats> {
        return entries.iterator()
    }

    fun get(index: Int): Stats {
        return entries[index]
    }

    fun size(): Int {
        return entries.size
    }

}