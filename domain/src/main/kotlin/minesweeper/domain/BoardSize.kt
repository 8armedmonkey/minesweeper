package minesweeper.domain

data class BoardSize(
    val width: Int,
    val height: Int
) {

    init {
        if (width <= 0 || height <= 0) {
            throw IllegalArgumentException()
        }
    }

    val numberOfCells: Int
        get() = width * height

}