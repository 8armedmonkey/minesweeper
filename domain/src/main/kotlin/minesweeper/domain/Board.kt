package minesweeper.domain

class Board private constructor(
    val size: BoardSize,
    val numberOfMines: Int
) {

    val status: Status
        get() = when {
            hasBeenCompleted -> Status.COMPLETED
            hasBeenExploded -> Status.EXPLODED
            else -> Status.INCOMPLETE
        }

    val hasBeenCompleted: Boolean
        get() {
            return cells.count { !it.hasMine && it.opened } == (size.numberOfCells - numberOfMines)
        }

    val hasBeenExploded: Boolean
        get() {
            return cells.find { it.hasMine && it.opened } != null
        }

    private val cells: MutableList<Cell> = mutableListOf()

    fun init() {
        populateCells()
        connectCells()
    }

    fun openCellAt(index: Int) {
        assertValidCellIndex(index)

        if (status == Status.INCOMPLETE) {
            cells[index].open()
        }
    }

    fun toggleFlagCellAt(index: Int) {
        assertValidCellIndex(index)

        if (status == Status.INCOMPLETE) {
            cells[index].toggleFlag()
        }
    }

    fun cellInfoAt(index: Int): CellInfo {
        assertValidCellIndex(index)

        val cell = cells[index]
        return CellInfo(cell.hasMine, cell.opened, cell.flagged, cell.indicator)
    }

    internal fun cellAt(index: Int): Cell {
        assertValidCellIndex(index)
        return cells[index]
    }

    private fun generateMineIndices(): HashSet<Int> {
        return (0 until size.numberOfCells)
            .shuffled()
            .asSequence()
            .take(numberOfMines)
            .toHashSet()
    }

    private fun populateCells() {
        val mineIndices = generateMineIndices()

        cells.clear()

        (0 until size.numberOfCells).forEach {
            cells.add(
                Cell.Builder()
                    .hasMine(mineIndices.contains(it))
                    .build()
            )
        }
    }

    private fun connectCells() {
        (0 until size.height).forEach { row ->
            (0 until size.width).forEach { column ->
                val cellIndex = row * size.height + column

                if (column > 0) {
                    val westCellIndex = cellIndex - 1
                    Cell.Connector.westToEast(cells[westCellIndex], cells[cellIndex])
                }

                if (row > 0) {
                    val northCellIndex = (row - 1) * size.height + column
                    Cell.Connector.northToSouth(cells[northCellIndex], cells[cellIndex])

                    if (column > 0) {
                        val northWestCellIndex = (row - 1) * size.height + column - 1
                        Cell.Connector.northWestToSouthEast(cells[northWestCellIndex], cells[cellIndex])
                    }

                    if (column < size.width - 1) {
                        val northEastCellIndex = (row - 1) * size.height + column + 1
                        Cell.Connector.northEastToSouthWest(cells[northEastCellIndex], cells[cellIndex])
                    }
                }
            }
        }
    }

    private fun assertValidCellIndex(index: Int) {
        if (index < 0 || index >= cells.size) {
            throw InvalidCellException()
        }
    }

    enum class Status {

        INCOMPLETE, COMPLETED, EXPLODED

    }

    class Builder {

        private lateinit var size: BoardSize
        private var numberOfMines: Int = 0

        fun size(size: BoardSize): Builder {
            this.size = size
            return this
        }

        fun numberOfMines(numberOfMines: Int): Builder {
            this.numberOfMines = numberOfMines
            return this
        }

        fun build(): Board {
            if (numberOfMines > size.numberOfCells) {
                throw BoardCreationException()
            }
            return Board(size, numberOfMines)
        }

    }

}