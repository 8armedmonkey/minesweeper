package minesweeper.domain

class Difficulties(vararg difficulties: Difficulty) : Iterable<Difficulty> {

    private val entries: List<Difficulty> = difficulties.toList()

    override fun iterator(): Iterator<Difficulty> {
        return entries.iterator()
    }

    fun get(index: Int): Difficulty {
        return entries[index]
    }

    fun size(): Int {
        return entries.size
    }

}