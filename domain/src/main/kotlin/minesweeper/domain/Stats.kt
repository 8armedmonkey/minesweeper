package minesweeper.domain

data class Stats(
    val name: String,
    val completeTimeMillis: Long
)