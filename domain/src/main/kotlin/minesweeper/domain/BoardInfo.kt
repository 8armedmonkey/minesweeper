package minesweeper.domain

data class BoardInfo(
    val boardSize: BoardSize,
    val cells: List<CellInfo>
)