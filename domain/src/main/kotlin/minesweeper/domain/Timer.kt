package minesweeper.domain

interface Timer {

    val durationMillis: Long

    val remainingTimeMillis: Long

    val running: Boolean

    var onTimerTickCallback: ((Timer) -> Unit)?

    var onTimerExpiredCallback: ((Timer) -> Unit)?

    fun start()

    fun stop()

    fun reset()

}