package minesweeper.domain

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.lang.IllegalArgumentException

class BoardSizeTest {

    @Test
    fun constructor_negativeWidth_throwIllegalArgumentException() {
        assertThrows<IllegalArgumentException> {
            BoardSize(-1, 1)
        }
    }

    @Test
    fun constructor_zeroWidth_throwIllegalArgumentException() {
        assertThrows<IllegalArgumentException> {
            BoardSize(0, 1)
        }
    }

    @Test
    fun constructor_negativeHeight_throwIllegalArgumentException() {
        assertThrows<IllegalArgumentException> {
            BoardSize(1, -1)
        }
    }

    @Test
    fun constructor_zeroHeight_throwIllegalArgumentException() {
        assertThrows<IllegalArgumentException> {
            BoardSize(1, 0)
        }
    }

    @Test
    fun numberOfCells_returnCorrectNumberOfCells() {
        val width = 8
        val height = 12
        val expectedNumberOfCells = width * height

        assertEquals(expectedNumberOfCells, BoardSize(width, height).numberOfCells)
    }

}