package minesweeper.domain

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class CellConnectorTest {

    @Test
    fun northToSouth_cellsAreConnected() {
        val north = Cell.Builder().build()
        val south = Cell.Builder().build()

        Cell.Connector.northToSouth(north, south)

        assertEquals(north, south.neighborAt(Cell.NeighborAt.NORTH))
        assertEquals(south, north.neighborAt(Cell.NeighborAt.SOUTH))
    }

    @Test
    fun westToEast_cellsAreConnected() {
        val west = Cell.Builder().build()
        val east = Cell.Builder().build()

        Cell.Connector.westToEast(west, east)

        assertEquals(west, east.neighborAt(Cell.NeighborAt.WEST))
        assertEquals(east, west.neighborAt(Cell.NeighborAt.EAST))
    }

    @Test
    fun northWestToSouthEast_cellsAreConnected() {
        val northWest = Cell.Builder().build()
        val southEast = Cell.Builder().build()

        Cell.Connector.northWestToSouthEast(northWest, southEast)

        assertEquals(northWest, southEast.neighborAt(Cell.NeighborAt.NORTH_WEST))
        assertEquals(southEast, northWest.neighborAt(Cell.NeighborAt.SOUTH_EAST))
    }

    @Test
    fun northEastToSouthWest_cellsAreConnected() {
        val northEast = Cell.Builder().build()
        val southWest = Cell.Builder().build()

        Cell.Connector.northEastToSouthWest(northEast, southWest)

        assertEquals(northEast, southWest.neighborAt(Cell.NeighborAt.NORTH_EAST))
        assertEquals(southWest, northEast.neighborAt(Cell.NeighborAt.SOUTH_WEST))
    }

}