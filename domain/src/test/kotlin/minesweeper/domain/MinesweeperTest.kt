package minesweeper.domain

import com.nhaarman.mockito_kotlin.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class MinesweeperTest {

    companion object {
        private val DIFFICULTY_EASY: Difficulty = Difficulty("Easy", BoardSize(3, 3), 1, 10000)
        private val DIFFICULTY_NORMAL = Difficulty("Normal", BoardSize(3, 3), 2, 30000)
        private val DIFFICULTY_HARD: Difficulty = Difficulty("Hard", BoardSize(3, 3), 8, 1000)
    }

    @Mock
    private lateinit var timerFactory: TimerFactory

    @Mock
    private lateinit var timer: Timer

    private lateinit var minesweeper: Minesweeper

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        whenever(timerFactory.createTimer(anyLong())).thenReturn(timer)

        whenever(timer.start()).doAnswer {
            whenever(timer.running).thenReturn(true)
            null
        }

        setupTimerWithRemainingTime()

        whenever(timer.stop()).doAnswer {
            whenever(timer.running).thenReturn(false)
            null
        }

        minesweeper = Minesweeper.Builder()
            .difficulties(Difficulties(
                DIFFICULTY_EASY,
                DIFFICULTY_NORMAL,
                DIFFICULTY_HARD
            ))
            .timerFactory(timerFactory)
            .build()
    }

    @Test
    fun init_withDifficultyThatDoesNotExist_throwInvalidDifficultyException() {
        assertThrows<InvalidDifficultyException> {
            minesweeper.init(Difficulty("Meh", BoardSize(2, 2), 1, 3600000))
        }
    }

    @Test
    fun start_alreadyInitialized_timerStarted() {
        minesweeper.init(DIFFICULTY_EASY)
        minesweeper.start()

        verify(timer).start()
    }

    @Test
    fun start_notYetInitialized_throwNotInitializedException() {
        assertThrows<NotInitializedException> {
            minesweeper.start()
        }
    }

    @Test
    fun start_statusIsWon_timerIsNotStarted() {
        minesweeper.init(DIFFICULTY_EASY)
        minesweeper.start()
        completeBoard()

        // Second attempt to start the timer, it should not start the timer.
        minesweeper.start()

        verify(timer, times(1)).start()
    }

    @Test
    fun start_statusIsLost_timerIsNotStarted() {
        minesweeper.init(DIFFICULTY_HARD)
        minesweeper.start()
        openCellWithMine()

        // Second attempt to start the timer, it should not start the timer.
        minesweeper.start()

        verify(timer, times(1)).start()
    }

    @Test
    fun stop_alreadyInitializedAndStarted_timerStopped() {
        minesweeper.init(DIFFICULTY_EASY)
        minesweeper.start()
        minesweeper.stop()

        verify(timer).stop()
    }

    @Test
    fun stop_alreadyInitializedButNotStarted_nothingHappens() {
        minesweeper.init(DIFFICULTY_EASY)
        minesweeper.stop()

        verify(timer, never()).stop()
    }

    @Test
    fun stop_notYetInitialized_throwNotInitializedException() {
        assertThrows<NotInitializedException> {
            minesweeper.stop()
        }
    }

    @Test
    fun reset_boardIsReinitialized() {
        minesweeper.init(DIFFICULTY_NORMAL)
        val beforeReset = minesweeper.boardInfo.cells.toTypedArray()

        minesweeper.reset()
        val afterReset = minesweeper.boardInfo.cells.toTypedArray()

        val similar = beforeReset.indices.fold(true) { accumulator, index ->
            accumulator && (beforeReset[index] == afterReset[index])
        }

        assertFalse(similar)
    }

    @Test
    fun reset_timerIsReset() {
        minesweeper.init(DIFFICULTY_EASY)
        minesweeper.reset()

        verify(timer).reset()
    }

    @Test
    fun openCellAt_alreadyInitializedAndStarted_cellIsOpened() {
        minesweeper.init(DIFFICULTY_EASY)
        minesweeper.start()

        minesweeper.openCellAt(0)

        assertTrue(minesweeper.boardInfo.cells[0].opened)
    }

    @Test
    fun openCellAt_alreadyInitializedButNotYetStarted_nothingHappens() {
        minesweeper.init(DIFFICULTY_EASY)

        minesweeper.openCellAt(0)

        assertFalse(minesweeper.boardInfo.cells[0].opened)
    }

    @Test
    fun openCellAt_notYetInitialized_throwNotInitializedException() {
        assertThrows<NotInitializedException> {
            minesweeper.openCellAt(0)
        }
    }

    @Test
    fun openCellAt_boardCompleted_timerIsStopped() {
        minesweeper.init(DIFFICULTY_EASY)
        minesweeper.start()

        completeBoard()

        verify(timer).stop()
    }

    @Test
    fun toggleFlagCellAt_alreadyInitializedAndStarted_cellFlagIsToggled() {
        minesweeper.init(DIFFICULTY_EASY)
        minesweeper.start()

        minesweeper.toggleFlagCellAt(0)

        assertTrue(minesweeper.boardInfo.cells[0].flagged)
    }

    @Test
    fun toggleFlagCellAt_alreadyInitializedButNotYetStarted_nothingHappens() {
        minesweeper.init(DIFFICULTY_EASY)

        minesweeper.toggleFlagCellAt(0)

        assertFalse(minesweeper.boardInfo.cells[0].flagged)
    }

    @Test
    fun toggleFlagCellAt_notYetInitialized_throwNotInitializedException() {
        assertThrows<NotInitializedException> {
            minesweeper.toggleFlagCellAt(0)
        }
    }

    @Test
    fun getStatus_notYetInitialized_throwNotInitializedException() {
        assertThrows<NotInitializedException> {
            minesweeper.status
        }
    }

    @Test
    fun getStatus_initializedAndTimerRemainingTimeGtZero_returnPlaying() {
        minesweeper.init(DIFFICULTY_EASY)

        assertEquals(Minesweeper.Status.PLAYING, minesweeper.status)
    }

    @Test
    fun getStatus_initializedAndTimerRemainingTimeEqZero_returnLost() {
        setupTimerWithoutRemainingTime()
        minesweeper.init(DIFFICULTY_EASY)

        assertEquals(Minesweeper.Status.LOST, minesweeper.status)
    }

    @Test
    fun getStatus_initializedAndCellWithMineIsOpened_returnLost() {
        minesweeper.init(DIFFICULTY_HARD)
        minesweeper.start()

        openCellWithMine()

        assertEquals(Minesweeper.Status.LOST, minesweeper.status)
    }

    @Test
    fun getStatus_initializedAndBoardCompleted_returnWon() {
        minesweeper.init(DIFFICULTY_EASY)
        minesweeper.start()

        completeBoard()

        assertEquals(Minesweeper.Status.WON, minesweeper.status)
    }

    @Test
    fun getBoardInfo_notYetInitialized_throwNotInitializedException() {
        assertThrows<NotInitializedException> {
            minesweeper.boardInfo
        }
    }

    @Test
    fun getDurationMillis_notYetInitialized_throwNotInitializedException() {
        assertThrows<NotInitializedException> {
            minesweeper.durationMillis
        }
    }

    @Test
    fun getRemainingTimeMillis_notYetInitialized_throwNotInitializedException() {
        assertThrows<NotInitializedException> {
            minesweeper.remainingTimeMillis
        }
    }

    private fun setupTimerWithRemainingTime() {
        whenever(timer.remainingTimeMillis).thenReturn(1000)
    }

    private fun setupTimerWithoutRemainingTime() {
        whenever(timer.remainingTimeMillis).thenReturn(0)
    }

    private fun completeBoard() {
        (0 until minesweeper.boardInfo.boardSize.numberOfCells)
            .filter { !minesweeper.boardInfo.cells[it].hasMine }
            .map { minesweeper.openCellAt(it) }
    }

    private fun openCellWithMine() {
        val cellIndex = findNextCellWithMineIndex()

        if (cellIndex != null) {
            minesweeper.openCellAt(cellIndex)
        }
    }

    private fun findNextCellWithMineIndex(): Int? =
        (0 until minesweeper.boardInfo.boardSize.numberOfCells).find {
            minesweeper.boardInfo.cells[it].hasMine
        }

}