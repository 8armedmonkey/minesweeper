package minesweeper.domain

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class BoardBuilderTest {

    private lateinit var builder: Board.Builder

    @BeforeEach
    fun setUp() {
        builder = Board.Builder()
    }

    @Test
    fun build_sizeIsCorrect() {
        val expectedWidth = 8
        val expectedHeight = 12

        val board = builder
            .size(BoardSize(expectedWidth, expectedHeight))
            .build()

        assertEquals(expectedWidth, board.size.width)
        assertEquals(expectedHeight, board.size.height)
    }

    @Test
    fun build_numberOfMinesIsCorrect() {
        val expectedNumberOfMines = 10

        val board = builder
            .size(BoardSize(8, 12))
            .numberOfMines(expectedNumberOfMines)
            .build()

        assertEquals(expectedNumberOfMines, board.numberOfMines)
    }

    @Test
    fun build_numberOfMinesExceedsBoardSize_throwBoardCreationException() {
        assertThrows<BoardCreationException> {
            builder
                .size(BoardSize(8, 12))
                .numberOfMines(97)
                .build()
        }
    }

}