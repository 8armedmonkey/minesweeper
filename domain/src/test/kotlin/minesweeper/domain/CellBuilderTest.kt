package minesweeper.domain

import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class CellBuilderTest {

    @Test
    fun build_hasMineIsCorrect() {
        val c1 = Cell.Builder().hasMine(true).build()
        val c2 = Cell.Builder().hasMine(false).build()

        assertTrue(c1.hasMine)
        assertFalse(c2.hasMine)
    }

    @Test
    fun build_openedIsCorrect() {
        val c1 = Cell.Builder().opened(true).build()
        val c2 = Cell.Builder().opened(false).build()

        assertTrue(c1.opened)
        assertFalse(c2.opened)
    }

    @Test
    fun build_flaggedIsCorrect() {
        val c1 = Cell.Builder().flagged(true).build()
        val c2 = Cell.Builder().flagged(false).build()

        assertTrue(c1.flagged)
        assertFalse(c2.flagged)
    }

}