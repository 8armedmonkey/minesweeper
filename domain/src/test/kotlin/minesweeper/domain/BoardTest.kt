package minesweeper.domain

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class BoardTest {

    private lateinit var board: Board

    @BeforeEach
    fun setUp() {
        board = Board.Builder()
            .size(BoardSize(3, 3))
            .numberOfMines(4)
            .build()

        board.init()
    }

    @Test
    fun init_cellsAreInitialized() {
        (0 until board.size.numberOfCells).forEach {
            assertNotNull(board.cellAt(it))
        }
    }

    @Test
    fun init_correctNumberOfMineCellsAreCreated() {
        val count = (0 until board.size.numberOfCells).count {
            board.cellAt(it).hasMine
        }

        assertEquals(board.numberOfMines, count)
    }

    @Test
    fun init_cellsAreConnectedCorrectly() {
        val c0 = board.cellAt(0)
        val c1 = board.cellAt(1)
        val c2 = board.cellAt(2)
        val c3 = board.cellAt(3)
        val c4 = board.cellAt(4)
        val c5 = board.cellAt(5)
        val c6 = board.cellAt(6)
        val c7 = board.cellAt(7)
        val c8 = board.cellAt(8)

        assertEquals(3, c0.numberOfNeighbors)
        assertEquals(c3, c0.neighborAt(Cell.NeighborAt.SOUTH))
        assertEquals(c4, c0.neighborAt(Cell.NeighborAt.SOUTH_EAST))
        assertEquals(c1, c0.neighborAt(Cell.NeighborAt.EAST))

        assertEquals(5, c1.numberOfNeighbors)
        assertEquals(c0, c1.neighborAt(Cell.NeighborAt.WEST))
        assertEquals(c3, c1.neighborAt(Cell.NeighborAt.SOUTH_WEST))
        assertEquals(c4, c1.neighborAt(Cell.NeighborAt.SOUTH))
        assertEquals(c5, c1.neighborAt(Cell.NeighborAt.SOUTH_EAST))
        assertEquals(c2, c1.neighborAt(Cell.NeighborAt.EAST))

        assertEquals(3, c2.numberOfNeighbors)
        assertEquals(c1, c2.neighborAt(Cell.NeighborAt.WEST))
        assertEquals(c4, c2.neighborAt(Cell.NeighborAt.SOUTH_WEST))
        assertEquals(c5, c2.neighborAt(Cell.NeighborAt.SOUTH))

        assertEquals(5, c3.numberOfNeighbors)
        assertEquals(c6, c3.neighborAt(Cell.NeighborAt.SOUTH))
        assertEquals(c7, c3.neighborAt(Cell.NeighborAt.SOUTH_EAST))
        assertEquals(c4, c3.neighborAt(Cell.NeighborAt.EAST))
        assertEquals(c1, c3.neighborAt(Cell.NeighborAt.NORTH_EAST))
        assertEquals(c0, c3.neighborAt(Cell.NeighborAt.NORTH))

        assertEquals(8, c4.numberOfNeighbors)
        assertEquals(c1, c4.neighborAt(Cell.NeighborAt.NORTH))
        assertEquals(c0, c4.neighborAt(Cell.NeighborAt.NORTH_WEST))
        assertEquals(c3, c4.neighborAt(Cell.NeighborAt.WEST))
        assertEquals(c6, c4.neighborAt(Cell.NeighborAt.SOUTH_WEST))
        assertEquals(c7, c4.neighborAt(Cell.NeighborAt.SOUTH))
        assertEquals(c8, c4.neighborAt(Cell.NeighborAt.SOUTH_EAST))
        assertEquals(c5, c4.neighborAt(Cell.NeighborAt.EAST))
        assertEquals(c2, c4.neighborAt(Cell.NeighborAt.NORTH_EAST))

        assertEquals(5, c5.numberOfNeighbors)
        assertEquals(c2, c5.neighborAt(Cell.NeighborAt.NORTH))
        assertEquals(c1, c5.neighborAt(Cell.NeighborAt.NORTH_WEST))
        assertEquals(c4, c5.neighborAt(Cell.NeighborAt.WEST))
        assertEquals(c7, c5.neighborAt(Cell.NeighborAt.SOUTH_WEST))
        assertEquals(c8, c5.neighborAt(Cell.NeighborAt.SOUTH))

        assertEquals(3, c6.numberOfNeighbors)
        assertEquals(c7, c6.neighborAt(Cell.NeighborAt.EAST))
        assertEquals(c4, c6.neighborAt(Cell.NeighborAt.NORTH_EAST))
        assertEquals(c3, c6.neighborAt(Cell.NeighborAt.NORTH))

        assertEquals(5, c7.numberOfNeighbors)
        assertEquals(c8, c7.neighborAt(Cell.NeighborAt.EAST))
        assertEquals(c5, c7.neighborAt(Cell.NeighborAt.NORTH_EAST))
        assertEquals(c4, c7.neighborAt(Cell.NeighborAt.NORTH))
        assertEquals(c3, c7.neighborAt(Cell.NeighborAt.NORTH_WEST))
        assertEquals(c6, c7.neighborAt(Cell.NeighborAt.WEST))

        assertEquals(3, c8.numberOfNeighbors)
        assertEquals(c5, c8.neighborAt(Cell.NeighborAt.NORTH))
        assertEquals(c4, c8.neighborAt(Cell.NeighborAt.NORTH_WEST))
        assertEquals(c7, c8.neighborAt(Cell.NeighborAt.WEST))
    }

    @Test
    fun openCellAt_index_cellAtIndexIsOpened() {
        board.openCellAt(4)

        assertTrue(board.cellAt(4).opened)
    }

    @Test
    fun openCellAt_indexLtZero_throwInvalidCellException() {
        assertThrows<InvalidCellException> {
            board.openCellAt(-1)
        }
    }

    @Test
    fun openCellAt_indexGteNumberOfCells_throwInvalidCellException() {
        assertThrows<InvalidCellException> {
            board.openCellAt(board.size.numberOfCells)
        }
    }

    @Test
    fun openCellAt_hasBeenCompleted_nothingHappens() {
        completeBoard()

        // Attempt to open an unopened cell after the board is completed.
        val cellIndex = findNextUnopenedCellIndex()

        if (cellIndex != null) {
            board.openCellAt(cellIndex)

            // The cell should not be opened.
            assertFalse(board.cellAt(cellIndex).opened)
        } else {
            fail("Wtf")
        }
    }

    @Test
    fun openCellAt_hasBeenExploded_nothingHappens() {
        openCellWithMine()

        // Attempt to open an unopened cell after the board is completed.
        val cellIndex = findNextUnopenedCellIndex()

        if (cellIndex != null) {
            board.openCellAt(cellIndex)

            // The cell should not be opened.
            assertFalse(board.cellAt(cellIndex).opened)
        } else {
            fail("Wtf")
        }
    }

    @Test
    fun toggleFlagCellAt_index_cellAtIndexIsFlagged() {
        board.toggleFlagCellAt(4)

        assertTrue(board.cellAt(4).flagged)
    }

    @Test
    fun toggleFlagCellAt_indexLtZero_throwInvalidCellException() {
        assertThrows<InvalidCellException> {
            board.toggleFlagCellAt(-1)
        }
    }

    @Test
    fun toggleFlagCellAt_indexGteNumberOfCells_throwInvalidCellException() {
        assertThrows<InvalidCellException> {
            board.toggleFlagCellAt(board.size.numberOfCells)
        }
    }

    @Test
    fun toggleFlagCellAt_hasBeenCompleted_nothingHappens() {
        completeBoard()

        // Attempt to open an unopened cell after the board is completed.
        val cellIndex = findNextUnopenedCellIndex()

        if (cellIndex != null) {
            board.toggleFlagCellAt(cellIndex)

            // The cell should not be opened.
            assertFalse(board.cellAt(cellIndex).flagged)
        } else {
            fail("Wtf")
        }
    }

    @Test
    fun toggleFlagCellAt_hasBeenExploded_nothingHappens() {
        openCellWithMine()

        // Attempt to open an unopened cell after the board is completed.
        val cellIndex = findNextUnopenedCellIndex()

        if (cellIndex != null) {
            board.toggleFlagCellAt(cellIndex)

            // The cell should not be opened.
            assertFalse(board.cellAt(cellIndex).flagged)
        } else {
            fail("Wtf")
        }
    }

    @Test
    fun cellInfoAt_returnCorrectCellInfo() {
        val cellWithoutMineIndex = findNextCellWithoutMineIndex()
        val cellWithMineIndex = findNextCellWithMineIndex()

        if (cellWithoutMineIndex != null && cellWithMineIndex != null) {
            board.openCellAt(cellWithoutMineIndex)
            board.toggleFlagCellAt(cellWithMineIndex)

            assertTrue(board.cellInfoAt(cellWithoutMineIndex).opened)
            assertTrue(board.cellInfoAt(cellWithMineIndex).flagged)
        } else {
            fail("Wtf")
        }
    }

    @Test
    fun cellAt_indexLtZero_throwInvalidCellException() {
        assertThrows<InvalidCellException> {
            board.cellAt(-1)
        }
    }

    @Test
    fun cellAt_indexGteNumberOfCells_throwInvalidCellException() {
        assertThrows<InvalidCellException> {
            board.cellAt(board.size.numberOfCells)
        }
    }

    @Test
    fun hasBeenCompleted_notAllCellsWithoutMinesAreOpened_returnFalse() {
        openCellWithoutMine()

        assertFalse(board.hasBeenCompleted)
    }

    @Test
    fun hasBeenCompleted_allCellsWithoutMinesAreOpened_returnTrue() {
        completeBoard()

        assertTrue(board.hasBeenCompleted)
    }

    @Test
    fun hasBeenExploded_noCellWithMineIsOpened_returnFalse() {
        openCellWithoutMine()

        assertFalse(board.hasBeenExploded)
    }

    @Test
    fun hasBeenExploded_aCellWithMineIsOpened_returnTrue() {
        openCellWithMine()

        assertTrue(board.hasBeenExploded)
    }

    @Test
    fun getStatus_hasNotBeenCompletedAndHasNotBeenExploded_returnIncomplete() {
        assertEquals(Board.Status.INCOMPLETE, board.status)
    }

    @Test
    fun getStatus_hasBeenCompleted_returnCompleted() {
        completeBoard()

        assertEquals(Board.Status.COMPLETED, board.status)
    }

    @Test
    fun getStatus_hasBeenExploded_returnExploded() {
        openCellWithMine()

        assertEquals(Board.Status.EXPLODED, board.status)
    }

    private fun completeBoard() {
        (0 until board.size.numberOfCells)
            .filter { !board.cellAt(it).hasMine }
            .map { board.openCellAt(it) }
    }

    private fun openCellWithMine() {
        val cellIndex = findNextCellWithMineIndex()

        if (cellIndex != null) {
            board.openCellAt(cellIndex)
        }
    }

    private fun openCellWithoutMine() {
        val cellIndex = findNextCellWithoutMineIndex()

        if (cellIndex != null) {
            board.openCellAt(cellIndex)
        }
    }

    private fun findNextUnopenedCellIndex(): Int? =
        (0 until board.size.numberOfCells).find {
            !board.cellAt(it).opened
        }

    private fun findNextCellWithoutMineIndex(): Int? =
        (0 until board.size.numberOfCells).find {
            !board.cellAt(it).hasMine
        }

    private fun findNextCellWithMineIndex(): Int? =
        (0 until board.size.numberOfCells).find {
            board.cellAt(it).hasMine
        }

}