package minesweeper.domain

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class LeaderBoardTest {

    @Test
    fun constructor_statsAreAdded() {
        val s1 = Stats("Foo", 1000)
        val s2 = Stats("Bar", 2000)
        val s3 = Stats("Baz", 3000)

        val iterator = LeaderBoard(s1, s2, s3).iterator()

        assertEquals(s1, iterator.next())
        assertEquals(s2, iterator.next())
        assertEquals(s3, iterator.next())
    }

    @Test
    fun get_index_correctStatsReturned() {
        val s1 = Stats("Foo", 1000)
        val s2 = Stats("Bar", 2000)
        val s3 = Stats("Baz", 3000)

        val leaderBoard = LeaderBoard(s1, s2, s3)

        assertEquals(s1, leaderBoard.get(0))
        assertEquals(s2, leaderBoard.get(1))
        assertEquals(s3, leaderBoard.get(2))
    }

    @Test
    fun size_returnCorrectNumberOfElements() {
        val s1 = Stats("Foo", 1000)
        val s2 = Stats("Bar", 2000)
        val s3 = Stats("Baz", 3000)

        assertEquals(3, LeaderBoard(s1, s2, s3).size())
    }

}