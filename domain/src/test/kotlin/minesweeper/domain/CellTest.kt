package minesweeper.domain

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class CellTest {

    @Test
    fun indicator_noNeighborsHaveMines_returnCorrectIndications() {
        val c1 = Cell.Builder().build()
        val c2 = Cell.Builder().build()
        val c3 = Cell.Builder().build()

        Cell.Connector.northToSouth(c1, c2)
        Cell.Connector.westToEast(c1, c3)

        assertEquals(0, c1.indicator)
    }

    @Test
    fun indicator_neighborsHaveMines_returnCorrectIndications() {
        val c1 = Cell.Builder().build()
        val c2 = Cell.Builder().hasMine(true).build()
        val c3 = Cell.Builder().build()
        val c4 = Cell.Builder().hasMine(true).build()

        Cell.Connector.northToSouth(c1, c2)
        Cell.Connector.westToEast(c1, c3)
        Cell.Connector.northWestToSouthEast(c1, c4)

        assertEquals(2, c1.indicator)
    }

    @Test
    fun toggleFlag_flagIsToggled() {
        val c = Cell.Builder().flagged(false).build()

        c.toggleFlag()
        assertTrue(c.flagged)

        c.toggleFlag()
        assertFalse(c.flagged)
    }

    @Test
    fun open_emptyCell_triggerEmptyNeighborsToOpen() {
        val c1 = Cell.Builder().build()
        val c2 = Cell.Builder().build()

        Cell.Connector.westToEast(c1, c2)
        c1.open()

        assertTrue(c1.opened)
        assertTrue(c2.opened)
    }

    @Test
    fun open_emptyCell_triggerIndicatorNeighborsToOpen() {
        val c1 = Cell.Builder().build()
        val c2 = Cell.Builder().build()
        val c3 = Cell.Builder().hasMine(true).build()

        Cell.Connector.westToEast(c1, c2)
        Cell.Connector.westToEast(c2, c3)
        c1.open()

        assertTrue(c1.opened)
        assertTrue(c2.opened)
    }

    @Test
    fun open_indicatorCell_doesNotTriggerNeighborsToOpen() {
        val c1 = Cell.Builder().build()
        val c2 = Cell.Builder().build()
        val c3 = Cell.Builder().hasMine(true).build()

        Cell.Connector.westToEast(c1, c2)
        Cell.Connector.westToEast(c2, c3)
        c2.open()

        assertFalse(c1.opened)
        assertTrue(c2.opened)
        assertFalse(c3.opened)
    }

    @Test
    fun open_mineCell_doesNotTriggerNeighborsToOpen() {
        val c1 = Cell.Builder().hasMine(true).build()
        val c2 = Cell.Builder().build()

        Cell.Connector.westToEast(c1, c2)
        c1.open()

        assertTrue(c1.opened)
        assertFalse(c2.opened)
    }

}